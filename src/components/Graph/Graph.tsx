import React, { useRef, useEffect, useState, useCallback, useMemo, useLayoutEffect } from 'react';
import * as t from 'io-ts';
import * as d3Selection from 'd3-selection';
import * as d3Zoom from 'd3-zoom';
import styled from 'styled-components';
import ReactTooltip from 'react-tooltip';
import { GraphContext, GraphContextCallback } from './GraphContext';
import { GraphNode } from './GraphNode/GraphNode';

const ZoomWrapper = styled.div`
    height: 100%;
    overflow: hidden;
`;

const GraphWrapper = styled.div`
    padding: 2em;
    transform: translate(0, 0) scale(1);
    transform-origin: 0 0;
`;

const Tooltip = styled(ReactTooltip)`
    &&& {
        padding: 0.75em 1.5em;
        color: inherit;
        font-size: 80%;

        .multi-line {
            display: flex;
            align-items: center;
            height: 1.5em;
            padding: 0;
            text-align: start;
        }
    }
`;

interface Transform {
    x: number;
    y: number;
    k: number;
}

interface TooltipPosition {
    left: number;
    top: number;
}

export const graphTooltipId = 'graphTooltip';

export interface GraphNodeValue {
    id: number;
    name: string;
    description?: string;
    isCollapsed?: boolean;
    childNodes?: GraphNodeValue[];
}

export const GraphNodeValueIO: t.Type<GraphNodeValue> = t.recursion('GraphNodeValueIO', () => {
    return t.intersection([
        t.type({
            id: t.number,
            name: t.string,
        }),
        t.partial({
            description: t.string,
            isCollapsed: t.boolean,
            childNodes: t.array(GraphNodeValueIO),
        }),
    ]);
});

export interface GraphProps {
    rootNode: GraphNodeValue;
}

export const Graph: React.FC<GraphProps> = ({ rootNode }) => {
    const defaultCollapsedNodes = useMemo<GraphNodeValue[]>(() => {
        const reducer = (accumulator: GraphNodeValue[], node: GraphNodeValue): GraphNodeValue[] => {
            if (node.isCollapsed) {
                accumulator.push(node);
            }

            if (node.childNodes?.length) {
                node.childNodes.reduce(reducer, accumulator);
            }

            return accumulator;
        };

        return [rootNode].reduce(reducer, []);
    }, [rootNode]);

    const [collapsedNodes, setCollapsedNodes] = useState<GraphNodeValue[]>(defaultCollapsedNodes);
    const [transform] = useState<Transform>({ x: 0, y: 0, k: 1 });

    const zoomWrapperRef = useRef<HTMLDivElement>(null);
    const graphWrapperRef = useRef<HTMLDivElement>(null);

    const toggleNode = useCallback<GraphContextCallback>(
        (node) => {
            const newCollapsedNodes = [...collapsedNodes];

            const nodeIndex = newCollapsedNodes.indexOf(node);
            if (nodeIndex === -1) {
                newCollapsedNodes.push(node);
            } else {
                newCollapsedNodes.splice(nodeIndex, 1);
            }

            setCollapsedNodes(newCollapsedNodes);
        },
        [collapsedNodes],
    );

    useEffect(ReactTooltip.rebuild, [rootNode]);

    useEffect(() => {
        const zoomWrapper = zoomWrapperRef.current;
        if (!zoomWrapper) {
            return;
        }

        const graphWrapper = graphWrapperRef.current;
        if (!graphWrapper) {
            return;
        }

        const zoomed = (): void => {
            const { x, y, k } = d3Zoom.zoomTransform(zoomWrapper);

            graphWrapper.style.transform = `translate(${x}px, ${y}px) scale(${k})`;

            transform.x = x;
            transform.y = y;
            transform.k = k;
        };

        const zoom = d3Zoom.zoom<HTMLDivElement, {}>().on('zoom', zoomed);

        d3Selection.select<HTMLDivElement, {}>(zoomWrapper).call(zoom);
    }, [transform]);

    useLayoutEffect(() => {
        setCollapsedNodes(defaultCollapsedNodes);
    }, [defaultCollapsedNodes]);

    const handleTooltipOverridePosition = ({ left, top }: TooltipPosition): TooltipPosition => {
        return {
            left: left / transform.k,
            top: top / transform.k,
        };
    };

    return (
        <ZoomWrapper ref={zoomWrapperRef}>
            <GraphWrapper ref={graphWrapperRef}>
                <GraphContext.Provider value={{ collapsedNodes, toggleNode }}>
                    <GraphNode isRoot value={rootNode} />
                </GraphContext.Provider>
                <Tooltip
                    multiline
                    id={graphTooltipId}
                    effect="solid"
                    overridePosition={handleTooltipOverridePosition}
                />
            </GraphWrapper>
        </ZoomWrapper>
    );
};
