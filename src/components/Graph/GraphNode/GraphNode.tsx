import React, { useRef, useEffect, useContext } from 'react';
import styled from 'styled-components';
import { GraphNodeValue, graphTooltipId } from '../Graph';
import { GraphContext } from '../GraphContext';

const contentWrapperMargin = '3em';

const connectorWidth = '2px';
const connectorColor = '#484e65';

const connectorArrowWidth = '6px';

const connectorPointWidth = '10px';
const connectorPointBorderWidth = '2px';

const List = styled.ul`
    display: flex;
    flex-direction: column;
    list-style-type: none;
    margin: 0;
    padding: 0;
`;

const ListItem = styled.li`
    display: flex;
    align-items: center;
    position: relative;
    white-space: nowrap;

    &:not(:only-child)::before {
        content: '';
        position: absolute;
        left: calc(-${connectorWidth} / 2);
        width: ${connectorWidth};
        height: 100%;
        background-color: ${connectorColor};
    }

    &:first-child::before,
    &:last-child::before {
        height: calc(50% + ${connectorWidth} / 2);
    }

    &:first-child::before {
        top: calc(50% - ${connectorWidth} / 2);
    }

    &:last-child::before {
        bottom: calc(50% - ${connectorWidth} / 2);
    }
`;

interface ContentWrapperProps {
    isRoot?: boolean;
    isLeaf?: boolean;
}

const ContentWrapper = styled.div<ContentWrapperProps>`
    position: relative;
    margin: ${contentWrapperMargin};

    &::before,
    &::after {
        content: '';
        position: absolute;
        top: calc(50% - ${connectorWidth} / 2);
        width: ${contentWrapperMargin};
        height: ${connectorWidth};
        background-color: ${connectorColor};
    }

    &::before {
        display: ${({ isRoot }) => isRoot && 'none'};
        left: -${contentWrapperMargin};
    }

    &::after {
        display: ${({ isLeaf }) => isLeaf && 'none'};
        right: -${contentWrapperMargin};
    }
`;

interface ConnectorPointProps {
    isTarget?: boolean;
}

const ConnectorPoint = styled.div<ConnectorPointProps>`
    position: absolute;
    top: calc(50% - ${connectorPointWidth} / 2);
    left: ${({ isTarget }) =>
        isTarget
            ? `calc((${connectorWidth} - ${connectorPointWidth}) / 2)`
            : `calc(100% - (${connectorWidth} + ${connectorPointWidth}) / 2)`};
    width: ${connectorPointWidth};
    height: ${connectorPointWidth};
    z-index: 10;
    background-color: #0baaf6;
    border: ${connectorPointBorderWidth} solid #202538;
    border-radius: 50%;
    box-shadow: 0 0 0 1px ${connectorColor};

    &::before {
        content: '';
        display: ${({ isTarget }) => !isTarget && 'none'};
        position: absolute;
        top: calc(-${connectorArrowWidth} / 2 + (${connectorPointWidth} - ${connectorPointBorderWidth} * 2) / 2);
        left: calc(-${connectorArrowWidth} - ${connectorPointBorderWidth});
        border-left: ${connectorArrowWidth} solid ${connectorColor};
        border-top: calc(${connectorArrowWidth} / 2) solid transparent;
        border-bottom: calc(${connectorArrowWidth} / 2) solid transparent;
    }
`;

const Content = styled.div`
    display: flex;
    align-items: center;
    height: 5.5em;
    padding: 0 3em;
    overflow: hidden;
    background-color: #141828;
    border: ${connectorWidth} solid ${connectorColor};
    border-radius: 0.75em;
    cursor: pointer;

    &:hover {
        border-color: #205da5;
    }
`;

export interface GraphNodeProps {
    value: GraphNodeValue;
    isRoot?: boolean;
}

export const GraphNode: React.FC<GraphNodeProps> = ({ value, isRoot }) => {
    const { name, description, childNodes } = value;

    const { collapsedNodes, toggleNode } = useContext(GraphContext);

    const listItemRef = useRef<HTMLLIElement>(null);

    const hasChildNodes = Boolean(childNodes?.length);

    const isCollapsed = collapsedNodes.includes(value);
    const isLeaf = !hasChildNodes || isCollapsed;

    useEffect(() => {
        const listItem = listItemRef.current;
        if (!listItem) {
            return;
        }

        listItem.style.minHeight = `${listItem.clientHeight}px`;
    }, [value, collapsedNodes]);

    const handleContentClick = (): void => {
        if (hasChildNodes) {
            toggleNode(value);
        }
    };

    const renderChildNode = (childNode: GraphNodeValue): React.ReactNode => {
        return <GraphNode key={childNode.id} value={childNode} />;
    };

    const renderChildNodes = (): React.ReactNode => {
        if (isLeaf) {
            return null;
        }

        if (!childNodes) {
            return null;
        }

        return <List>{childNodes.map(renderChildNode)}</List>;
    };

    const renderNode = (): React.ReactNode => {
        return (
            <ListItem ref={listItemRef}>
                <ContentWrapper isRoot={isRoot} isLeaf={isLeaf}>
                    {!isRoot && <ConnectorPoint isTarget />}
                    <Content data-for={graphTooltipId} data-tip={description} onClick={handleContentClick}>
                        {name}
                    </Content>
                    {hasChildNodes && <ConnectorPoint />}
                </ContentWrapper>
                {renderChildNodes()}
            </ListItem>
        );
    };

    if (isRoot) {
        return <List>{renderNode()}</List>;
    }

    return <>{renderNode()}</>;
};
