import React from 'react';
import { GraphNodeValue } from './Graph';

export type GraphContextCallback = (node: GraphNodeValue) => void;

export interface GraphContextValue {
    collapsedNodes: GraphNodeValue[];
    toggleNode: GraphContextCallback;
}

export const GraphContext = React.createContext<GraphContextValue>({
    collapsedNodes: [],
    toggleNode: () => undefined,
});
