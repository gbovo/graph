import React, { useState, useEffect } from 'react';
import styled from 'styled-components';
import AceEditor from 'react-ace';
import { ReactComponent as RunIcon } from '../../icons/run.svg';
import 'ace-builds/src-noconflict/mode-json';
import 'ace-builds/src-noconflict/theme-twilight';

const Wrapper = styled.div`
    display: flex;
    flex-direction: column;
    height: 100%;
`;

const Header = styled.div`
    display: flex;
    justify-content: space-between;
    height: 2em;
    background-color: #2a2a2a;
`;

interface MessageProps {
    isError?: boolean;
}

const Message = styled.small<MessageProps>`
    display: flex;
    align-items: center;
    padding: 0 1em;
    overflow: hidden;
    white-space: nowrap;
    text-overflow: ellipsis;
    color: ${({ isError }) => isError && '#d60100'};
`;

const RunButton = styled.button`
    display: flex;
    padding: 0 1em;
    background-color: transparent;
    border: none;
    outline: none;
    color: inherit;
    opacity: 0.5;
    cursor: pointer;

    &:hover,
    &:focus {
        opacity: 1;
    }
`;

const RunButtonIcon = styled(RunIcon)`
    width: 1em;
    height: 1em;
    fill: currentColor;
`;

export interface JsonEditorProps {
    value?: string;
    message?: string;
    isError?: boolean;
    onRun?: (value: string) => void;
}

export const JsonEditor: React.FC<JsonEditorProps> = ({ value: defaultValue, message, isError, onRun }) => {
    const [value, setValue] = useState<string>(defaultValue || '');

    useEffect(() => {
        setValue(defaultValue || '');
    }, [defaultValue]);

    const handleRunButtonClick = (): void => {
        if (onRun) {
            onRun(value);
        }
    };

    return (
        <Wrapper>
            <Header>
                <Message isError={isError}>{message}</Message>
                <RunButton type="button" title="Run" onClick={handleRunButtonClick}>
                    <RunButtonIcon />
                </RunButton>
            </Header>
            <AceEditor mode="json" theme="twilight" width="100%" height="100%" value={value} onChange={setValue} />
        </Wrapper>
    );
};
