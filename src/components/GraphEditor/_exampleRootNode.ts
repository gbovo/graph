import { GraphNodeValue } from '../Graph/Graph';

const exampleRootNode: GraphNodeValue = {
    id: 1,
    name: 'Root',
    description: 'Description',
    childNodes: [
        {
            id: 2,
            name: 'Foo',
            childNodes: [
                { id: 5, name: 'Foo Child 1' },
                { id: 6, name: 'Foo Child 2' },
            ],
        },
        { id: 3, name: 'Bar', isCollapsed: true, childNodes: [{ id: 7, name: 'Bar Child 1' }] },
        { id: 4, name: 'Baz', childNodes: [{ id: 8, name: 'Baz Child 1' }] },
    ],
};

export default exampleRootNode;
