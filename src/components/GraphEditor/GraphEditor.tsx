import React, { useState, useMemo } from 'react';
import styled from 'styled-components';
import { pipe } from 'fp-ts/lib/pipeable';
import { fold } from 'fp-ts/lib/Either';
import exampleRootNode from './_exampleRootNode';
import { JsonEditor } from '../JsonEditor/JsonEditor';
import { GraphNodeValue, Graph, GraphNodeValueIO } from '../Graph/Graph';

const Wrapper = styled.div`
    display: flex;
    height: 100%;
`;

interface ColumnProps {
    width?: string;
}

const Column = styled.div<ColumnProps>`
    width: ${({ width }) => width};
`;

export interface GraphEditorProps {
    withExample?: boolean;
}

export const GraphEditor: React.FC<GraphEditorProps> = ({ withExample }) => {
    const messageSuccess = 'Graph has been generated successfully';
    const messageError = 'Errors exist in JSON';

    const defaultJson = useMemo<string>(() => {
        if (!withExample) {
            return '';
        }

        const json = JSON.stringify(exampleRootNode, undefined, '\t');

        return `${json}\n`;
    }, [withExample]);

    const [json, setJson] = useState<string>(defaultJson);

    const rootNode = useMemo<GraphNodeValue | undefined>(() => {
        if (!json) {
            return;
        }

        const onLeft = (): GraphNodeValue | undefined => undefined;

        const onRight = (node: GraphNodeValue): GraphNodeValue | undefined => node;

        try {
            const rawNode = JSON.parse(json);

            return pipe(GraphNodeValueIO.decode(rawNode), fold(onLeft, onRight));
        } catch {}
    }, [json]);

    const message = useMemo<string | undefined>(() => {
        if (!json) {
            return;
        }

        return rootNode ? messageSuccess : messageError;
    }, [json, rootNode]);

    const isError = Boolean(json && !rootNode);

    return (
        <Wrapper>
            <Column width="40%">
                <JsonEditor value={json} message={message} isError={isError} onRun={setJson} />
            </Column>
            <Column width="60%">{rootNode && <Graph rootNode={rootNode} />}</Column>
        </Wrapper>
    );
};
