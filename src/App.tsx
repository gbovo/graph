import React from 'react';
import { createGlobalStyle } from 'styled-components';
import { normalize } from 'styled-normalize';
import { GraphEditor } from './components/GraphEditor/GraphEditor';

const GlobalStyle = createGlobalStyle`
    ${normalize}

    *,
    ::before,
    ::after {
        box-sizing: border-box;
    }

    html,
    body,
    #root {
        height: 100%;
    }

    body {
        background-color: #1b1e2c;
        color: #fff;
        font-family: Tahoma, Verdana, 'Segoe UI', sans-serif;
    }
`;

export const App: React.FC = () => {
    return (
        <>
            <GraphEditor withExample />
            <GlobalStyle />
        </>
    );
};
